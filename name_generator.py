import random

names = []
with open('name_bank.txt') as f:
    for line in f:
        names.append(line.rstrip())

def split(word):
    return [char for char in word]

def convert(s):
    new = ""

    for x in s:
        new += x
    
    return new

def generate_random_planet_name_list(planet_num):
    generated_planet_names = []

    count = 0
    while count < planet_num:
        
        random_word_num = random.randrange(0, len(names))
        random_word = names[random_word_num]
        random_to_split = random.randrange(0,2)
        if random_to_split:
            random_second_word_num = random.randrange(0, len(names))
            random_second_word = names[random_second_word_num].lower()
            #add additional first random_word splitting logic
        else:
            random_second_word = ''
        joined_word = random_word + random_second_word

        random_to_truncate = random.randrange(0,2)
        if random_to_truncate and len(joined_word) > 1:
            amount_to_truncate = random.randrange(1, len(joined_word))
            new_word = split(joined_word)
            trunc_count = 0
            while trunc_count < amount_to_truncate:
                new_word.pop()
                trunc_count += 1

            joined_word = convert(new_word)

        if len(joined_word) == 1:
            random_num = str(random.randrange(1, 10000))
            joined_word += " " + random_num

        generated_planet_names.append(joined_word)
        count += 1
    return generated_planet_names



generate_random_planet_name_list(100)