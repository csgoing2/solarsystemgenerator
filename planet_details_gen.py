from asyncio.windows_events import NULL
import enum
import random
from name_generator import generate_random_planet_name_list

global planet_type 
global solar_system
global planet_names
solar_system = []
planet_type = ['cold', 'temperate', 'hot']
planet_land_types = ['no_land', 'low_land', 'high_land']
planet_mountain_types = ['no_mountain', 'low_mountain', 'high_mountain']
planet_ice_types = ['no_ice', 'low_ice', 'high_ice']
planet_fire_types = ['no_fire', 'low_fire', 'high_fire']
planet_atmosphere_types = ['no_cloud', 'low_cloud', 'high_cloud']

planet_names = []
name_array_multiplier = 3

def generate_random():
    num_of_planets = random.randrange(1, 10)
    solar_system = []
    planet_names = generate_random_planet_name_list(num_of_planets**name_array_multiplier)
    count = 0
    while (count < num_of_planets):
        size_x = size_y = random.randrange(10, 30)
        planet_name_selection = random.randrange(0, len(planet_names))
        planet_name_select = planet_names[planet_name_selection]
        #planet layer logic
        #planet type - layer 1
        planet_type_selection = random.randrange(0, len(planet_type))
        planet_type_select = planet_type[planet_type_selection]
        match planet_type_selection:
            case 0: #cold
                #planet land - layer 2
                planet_land_selection = 0
                planet_land_select = planet_land_types[planet_land_selection]
                #planet mountains - layer 3
                planet_mountain_selection = random.randrange(0, len(planet_mountain_types))
                planet_mountain_select = planet_mountain_types[planet_mountain_selection]
                #planet ice - layer 4
                planet_ice_selection = random.randrange(0, len(planet_ice_types))
                planet_ice_select = planet_ice_types[planet_ice_selection]
                #planet fire - layer 5
                planet_fire_selection =  0
                planet_fire_select = planet_fire_types[planet_fire_selection]
                #planet atmosphere - layer 6
                planet_atmosphere_selection = random.randrange(0, len(planet_atmosphere_types))
                planet_atmosphere_select = planet_atmosphere_types[planet_atmosphere_selection]
            case 1: #temperate
                #planet land - layer 2
                planet_land_selection = random.randrange(0, len(planet_land_types))
                planet_land_select = planet_land_types[planet_land_selection]
                #planet mountains - layer 3
                planet_mountain_selection = random.randrange(0, len(planet_mountain_types))
                planet_mountain_select = planet_mountain_types[planet_mountain_selection]
                #planet ice - layer 4
                planet_ice_selection = 0
                planet_ice_select = planet_ice_types[planet_ice_selection]
                #planet fire - layer 5
                planet_fire_selection =  0
                planet_fire_select = planet_fire_types[planet_fire_selection]
                #planet atmosphere - layer 6
                planet_atmosphere_selection = random.randrange(0, len(planet_atmosphere_types))
                planet_atmosphere_select = planet_atmosphere_types[planet_atmosphere_selection]
            case 2: #hot
                #planet land - layer 2
                planet_land_selection = 0
                planet_land_select = planet_land_types[planet_land_selection]
                #planet mountains - layer 3
                planet_mountain_selection = random.randrange(0, len(planet_mountain_types))
                planet_mountain_select = planet_mountain_types[planet_mountain_selection]
                #planet ice - layer 4
                planet_ice_selection = 0
                planet_ice_select = planet_ice_types[planet_ice_selection]
                #planet fire - layer 5
                planet_fire_selection =  random.randrange(0, len(planet_fire_types))
                planet_fire_select = planet_fire_types[planet_fire_selection]
                #planet atmosphere - layer 6
                planet_atmosphere_selection = random.randrange(0, len(planet_atmosphere_types))
                planet_atmosphere_select = planet_atmosphere_types[planet_atmosphere_selection]
        """ #planet land - layer 2
        planet_land_selection = random.randrange(0, len(planet_land_types))
        planet_land_select = planet_land_types[planet_land_selection]
        #planet mountains - layer 3
        planet_mountain_selection = random.randrange(0, len(planet_mountain_types))
        planet_mountain_select = planet_mountain_types[planet_mountain_selection]
        #planet ice - layer 4
        planet_ice_selection = random.randrange(0, len(planet_ice_types))
        planet_ice_select = planet_ice_types[planet_ice_selection]
        #planet fire - layer 5
        planet_fire_selection =  random.randrange(0, len(planet_fire_types))
        planet_fire_select = planet_fire_types[planet_fire_selection]
        #planet atmosphere - layer 6
        planet_atmosphere_selection = random.randrange(0, len(planet_atmosphere_types))
        planet_atmosphere_select = planet_atmosphere_types[planet_atmosphere_selection] """
        

        planet = (planet_name_select, size_x, size_y, planet_type_selection, planet_land_selection, planet_mountain_selection, planet_ice_selection, planet_fire_selection, planet_atmosphere_selection)
        solar_system.append(planet)
        count += 1

    solar_system.insert(0, (planet_names[random.randrange(0, len(planet_names))],60,60,))
    return solar_system
    
generate_random()


