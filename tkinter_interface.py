from asyncio.windows_events import NULL
from cgitb import text
from ctypes import resize
from email.mime import base
import os
import tkinter
from tkinter import *
from tkinter import ttk
from turtle import back
import PIL
from PIL import Image, ImageOps
from PIL import ImageTk
from planet_details_gen import generate_random
import random
import fnmatch


import planet_details_gen

global system
system = []

global planet_images
planet_images = []

global planet_names
planet_names = []

global planet_colors
planet_colors = ['#c8c8c8', '#65adff', '#73371f'] #white, blue, brown

global planet_template
#open image to be resized
planet_template = PIL.Image.open("planet_body.png").convert('RGBA')

global planet_body_images
global land_images
global mountain_images
global ice_images
global atmosphere_images

planet_body_images = []
land_images = []
mountain_images = []
ice_images = []
fire_images = []
atmosphere_images = []




def recolor_image(image):
    image_to_recolor = image.convert("L")
    color_selection = random.randrange(0, len(planet_colors))
    color_select = planet_colors[color_selection]

    recolored_image = ImageOps.colorize(image_to_recolor, black = color_select, white="white")
    recolored_image.convert('RGBA')
    return recolored_image

def add_layers(image, scale_size, planet_details):
    #print(planet_details)
    layered_image = Image.new("RGBA", scale_size)
    base_image = image.convert('RGBA')
    s_x = s_y = scale_size[1]
    
    paste_x = paste_y = (s_x // s_y)
    paste_position = (paste_x, paste_y)

    layered_image.paste(image, (paste_position))
    #print(base_image.mode)
    generate_planet_layers(planet_details, layered_image, scale_size, paste_position)
    #print(planet_details)
    return layered_image

def get_size_multiplyer():
    size_multiplyer = int(size_input_text_box.get(1.0, END))              ##Continue working on. Figure how to check if text box is empty, if so set the size_multiplier to a fixed value.
    if size_input_text_box.get("1.0", END)=="\n":
        size_multiplyer = 5
        print("Empty Size Text")
    print(size_input_text_box.get(1.0, 'end-1c'))
    print(type(size_multiplyer))
    """ if type(size_multiplyer) != int or size_multiplyer <= 0 or size_multiplyer is NULL:
        size_multiplyer = 4
        #print(size_multiplyer, "if")
        return size_multiplyer
    else:
        #print(size_multiplyer, "else")
        return size_multiplyer
 """
def generate_planet_layers(planet_details, layered_image, scale_size, paste_position):
    land_rotation = random.randrange(0, 360)
    mountain_rotation = random.randrange(0, 360)
    fire_rotation = random.randrange(0, 360)
    atmo_rotation = random.randrange(0, 360)

    land_choice = planet_details[3]
    land_layer = land_images[land_choice].resize(scale_size).rotate(land_rotation)
    layered_image.paste(land_layer, (paste_position), mask=land_layer)

    mountain_choice = planet_details[4]
    mountain_layer = mountain_images[mountain_choice].resize(scale_size).rotate(mountain_rotation)
    layered_image.paste(mountain_layer, (paste_position), mask=mountain_layer)

    ice_choice = planet_details[5]
    ice_layer = ice_images[ice_choice].resize(scale_size)
    layered_image.paste(ice_layer, (paste_position), mask=ice_layer) 

    fire_choice = planet_details[6]
    fire_layer = fire_images[fire_choice].resize(scale_size).rotate(fire_rotation)
    layered_image.paste(fire_layer, (paste_position), mask=fire_layer)

    atmo_choice = planet_details[7]
    atmo_layer = atmosphere_images[atmo_choice].resize(scale_size).rotate(atmo_rotation)
    layered_image.paste(atmo_layer, (paste_position), mask=atmo_layer)

def generate_planet_labels(box):
    local_size_multiplyer = get_size_multiplyer()
    planet_images.clear()
    #generate tuple with planet data
    system = generate_random()


    #generate planet names based on len(system)
    #planet_names = generate_random_planet_name_list(len(system))

    #for each of the items in the system tuple
   
    for i in range(len(system)):
        planet_body_selection = random.randrange(0, len(planet_body_images))
        planet_body_select = planet_body_images[planet_body_selection]
        
        #extract size, x_size will always equal y_size
        x_size = y_size = system[i][1]
        size = (x_size* local_size_multiplyer, y_size* local_size_multiplyer) 
        #resized_p_image = planet_template.resize(size)
        resized_p_image = planet_body_select.resize(size)

        planet_image = ImageTk.PhotoImage(resized_p_image)

        if i >= 1:
            #recolored_p_image = recolor_image(resized_p_image)
            layered_p_image = add_layers(resized_p_image, size, system[i])
            
            #store resized image in planet_image"""
            planet_image = ImageTk.PhotoImage(layered_p_image) 

        
        #add resized image to planet_images list
        planet_images.append(planet_image)

        label = tkinter.Label(image=planet_images[i])
        label.grid(column=4+i, row=0)
    #delete contents of text box
    box.delete('1.0', END)
    #insert system text into text box
    for item in system:
        box.insert('1.0', str(item) + "\n")

def image_loader(image_set, file_string):
    if image_set != planet_body_images:
        empty_layer = PIL.Image.open("empty.png")
        image_set.append(empty_layer)

    list_name_prefix = file_string[:-1]
    count = 1
    
    for file in os.listdir("."):
        list_name = list_name_prefix + "_image" + str(count)
        if fnmatch.fnmatch(file, file_string):
            #if there is a match in file name
            #we want to open the image and store 
            image = PIL.Image.open(file)
            #assign image to new variable to store in list
            image_set.append(image)
            count += 1
            #print(list_name)

    print("Images loaded")
    print(image_set)
    return image_set

planet_body_images = image_loader(planet_body_images, 'body*')
land_images = image_loader(land_images, 'land*')
mountain_images = image_loader(mountain_images, 'mountain*')
ice_images = image_loader(ice_images, 'ice*')
fire_images = image_loader(fire_images, 'fire*')
atmosphere_images = image_loader(atmosphere_images, 'atmo*')

root = Tk()

label_text = 'Generate'
enlarge_amount_label = Label(text="Enlarge amount")
enlarge_amount_label.grid(column=0, row=1)

#label 1 procedure
#open image
im = PIL.Image.open("planet_body.png")
#convert image to PhotoImage
tkinter_comp_img = ImageTk.PhotoImage(im)
#instantiate label1 as label pointing to PhotoImage
label1 = tkinter.Label(image=tkinter_comp_img)

#Planet Name Text
text_box = Text(root, bg='#444444', height=15, width=45)
text_box.grid(column=2, row=0)
text_box.insert('1.0', "Welcome to the Planet Generator. \nYou can click Generate to create a set of planet names and images. \nYou can also enter a number to enlarge the images additionally.")

#size input text box sizing
size_input_text_box = Text(root, bg='#ffffff', height=3, width=3)
size_input_text_box.grid(column=0, row=0)

#frame handling
frm = ttk.Frame(root, padding=10)
grid = frm.grid()
#buttons
generate_button = ttk.Button(frm, text="Generate", command=lambda : generate_planet_labels(text_box) )
generate_button.grid(column=0, row=0)

quit_button = ttk.Button(frm, text="Quit", command=root.destroy)
quit_button.grid(column=1, row=0)
#labels
#set label 1 positoning
#label1.grid(column=3, row=0)
#root.geometry("700x400+100+300")

root.mainloop()

